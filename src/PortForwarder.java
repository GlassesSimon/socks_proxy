import org.xbill.DNS.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;

public class PortForwarder {
    int PORT;

    int size = 8192;
    ByteBuffer buf;

    private int ACC = 0;
    private int CON = 1;
    private int BOND = 2;

    private Map<SocketChannel, SocketChannel> connectionMap = new HashMap<>();
    private Map<SocketChannel, Integer> infoMap = new HashMap<>();
    private Map<Integer, Connection> dnsMap = new HashMap<>();


    private Selector selector;
    private Parser parser = new Parser();

    PortForwarder(Integer port) {
        PORT = port;
        buf = ByteBuffer.allocate(size);
    }

    public void start() {
        try {
            selector = Selector.open();
        } catch (IOException err) {
            err.printStackTrace();
        }

        SelectionKey key;
        String dnsServers[] = ResolverConfig.getCurrentConfig().servers();
        ByteBuffer messageBuffer = ByteBuffer.allocate(size);

        try (ServerSocketChannel serverChannel = ServerSocketChannel.open(); DatagramChannel udpChannel = DatagramChannel.open()){
            serverChannel.configureBlocking(false);
            serverChannel.socket().bind(new InetSocketAddress(InetAddress.getLocalHost(), PORT));
            serverChannel.register(selector, SelectionKey.OP_ACCEPT);

            udpChannel.configureBlocking(false);
            udpChannel.connect(new InetSocketAddress(dnsServers[0],53));
            udpChannel.register(selector, SelectionKey.OP_READ);

            while (true) {
                selector.select();
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectedKeys.iterator();
                while (iterator.hasNext()) {
                    key = iterator.next();
                    iterator.remove();

                    if (key.isValid()) {
                        if (key.isAcceptable()) {
                            SocketChannel sc = serverChannel.accept();
                            sc.configureBlocking(false);
                            sc.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE | SelectionKey.OP_CONNECT);
                            infoMap.put(sc, ACC);
                        }
                        if (key.isConnectable()) {
                            ((SocketChannel)key.channel()).finishConnect();
                        }
                        if (key.isReadable()) {
                            if (key.channel() instanceof SocketChannel) {
                                Integer info = infoMap.get(key.channel());

                                SocketChannel sc = (SocketChannel)key.channel();
                                int count = -1;
                                boolean flag;
                                if ((info == ACC) || (info == CON)) {
                                    try {
                                        count = sc.read(messageBuffer);
                                    } catch (IOException err){
                                        err.printStackTrace();
                                        close(key);
                                    }
                                    if (count < 0) {
                                        infoMap.remove(key.channel());
                                        close(key);
                                    }
                                    else {
                                        flag = parser.check(messageBuffer, info);
                                        if (flag) {
                                            if (info == ACC) {
                                                ByteBuffer outBuffer = parser.makeAcceptanceAnswer();
                                                sc.write(ByteBuffer.wrap(outBuffer.array(), 0, 2));
                                                infoMap.put(sc, CON);
                                            } else if (info == CON) {
                                                try {
                                                    InetAddress address = parser.getAddress(messageBuffer);
                                                    int serverPort = parser.getPort(messageBuffer, count);
                                                    if (connect(address, serverPort, sc, key)) {
                                                        infoMap.put(sc, BOND);
                                                    }
                                                } catch (MyException e) {
                                                    Name name = Name.fromString(parser.getDomain(messageBuffer), Name.root);
                                                    Record rec = Record.newRecord(name, Type.A, DClass.IN);
                                                    Message dns_message = Message.newQuery(rec);

                                                    udpChannel.write(ByteBuffer.wrap(dns_message.toWire()));
                                                    int port = parser.getPort(messageBuffer, count);
                                                    dnsMap.put(dns_message.getHeader().getID(), new Connection(port, sc));
                                                }
                                            }
                                            messageBuffer.clear();
                                        }
                                    }
                                } else if (info == BOND) {
                                    SocketChannel connect = connectionMap.get(sc);

                                    System.out.println("CONNECTION (SOURCE) : " + sc.toString() + sc.isConnected());
                                    System.out.println("CONNECTION (DEST) : " + connect.toString() + connect.isConnected());

                                    if (connect.isConnected()) {
                                        int count2;
                                        try {
                                            count2 = sc.read(messageBuffer);
                                            if (count2 == -1) {
                                                close(key);
                                            } else {
                                                System.out.println(count2);
                                                System.out.println(Arrays.toString(messageBuffer.array()));
                                                connect.write(ByteBuffer.wrap(messageBuffer.array(), 0, count2));
                                            }
                                        } catch (IOException err) {
                                            System.out.println("Connection is closed");
                                            close(key);
                                        }
                                    }
                                    messageBuffer.clear();
                                }
                            } else {
                                ByteBuffer buffer = ByteBuffer.allocate(1024);
                                int count3 = udpChannel.read(buffer);
                                if (count3 <= 0) {
                                    continue;
                                }
                                Message msg = new Message(buffer.array());
                                Record[] recs = msg.getSectionArray(1);
                                for (Record rec : recs) {
                                    if (rec instanceof ARecord) {
                                        ARecord aRec = (ARecord) rec;
                                        InetAddress address = aRec.getAddress();

                                        int id = msg.getHeader().getID();
                                        Connection connect = dnsMap.get(id);

                                        int port = connect.port;
                                        if (connect(address, port, dnsMap.get(id).socketChannel, key)) {
                                            infoMap.put(dnsMap.get(id).socketChannel, BOND);
                                        }

                                        dnsMap.remove(id);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception err){
            err.printStackTrace();
        }
    }

    private boolean connect(InetAddress address, int serverPort, SocketChannel sc, SelectionKey key) {
        SocketChannel connect;
        try {
            connect = SocketChannel.open(new InetSocketAddress(address, serverPort));
            ByteBuffer buffer = parser.makeConnectAnswer(PORT, connect.isConnected());
            if (!connect.isConnected()) {
                close(key);
                return connect.isConnected();
            }
            try {
                sc.write(ByteBuffer.wrap(buffer.array(), 0, 10));
            } catch (ClosedChannelException err){
                err.printStackTrace();
                return false;
            }
            connect.configureBlocking(false);
            connect.register(selector, SelectionKey.OP_READ | SelectionKey.OP_CONNECT);

            connectionMap.put(sc, connect);
            connectionMap.put(connect, sc);

            infoMap.put(connect, BOND);
            return connect.isConnected();
        } catch (IOException err) {
            err.printStackTrace();
            return false;
        }
    }

    private void close(SelectionKey key) {
        SocketChannel sc = connectionMap.get(key.channel());
        if (sc != null) {
            try {
                sc.close();
                connectionMap.remove(connectionMap.get(key.channel()));
                connectionMap.remove(key.channel());
            } catch (IOException err) {
                err.printStackTrace();
            }
        }
        try {
            key.channel().close();
        } catch (IOException err) {
            err.printStackTrace();
        }
    }
}