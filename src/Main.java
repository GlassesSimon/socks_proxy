public class Main {
    public static void main(String[] args) {
        if (args.length == 1){
            try {
                Integer PORT = Integer.parseInt(args[0]);
                new PortForwarder(PORT).start();
            } catch (Exception e){
                System.out.println("Invalid argument. Enter your port.");
                return;
            }
        }
        else {
            System.out.println("Invalid number of arguments. Enter your port.");

        }
    }
}