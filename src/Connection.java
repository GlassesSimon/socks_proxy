import java.nio.channels.SocketChannel;

public class Connection {
    int port;
    SocketChannel socketChannel;

    Connection(int p, SocketChannel sc){
        port = p;
        socketChannel = sc;
    }
}