import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class Parser {
    private int ACC = 0;
    private int CON = 1;
    private int BOND = 2;

    private byte AUTH = 0x00;
    private byte COMM = 0x01;
    private byte IPv4 = 0x01;
    private byte DOM = 0x03;
    private byte VERS = 0x05;

    public InetAddress getAddress(ByteBuffer message) throws MyException {
        byte[] msg = message.array();
        if (msg[3] == IPv4) {
            byte[] addr = new byte[] {msg[4], msg[5], msg[6], msg[7]};
            InetAddress address = null;
            try {
                address = InetAddress.getByAddress(addr);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            return address;
        }
        else {
            throw new MyException();
        }
    }

    public int getPort(ByteBuffer message, int length){
        byte[] msg = message.array();
        int destinationPort = (((0xFF & msg[length - 2]) << 8) + (0xFF & msg[length - 1]));
        System.out.println(destinationPort);
        return destinationPort;
    }

    public String getDomain(ByteBuffer message){
        int length = message.array()[4];
        byte[] domain = Arrays.copyOfRange(message.array(), 5, length + 5);
        return new String(domain);
    }

    public boolean check(ByteBuffer message, int state) {
        boolean flag = false;
        byte[] msg = message.array();

        if (state == ACC) {
            if (msg[0] != VERS) {
                return flag;
            }
            else {
                int num = message.get(1);
                boolean found = false;

                for (int i = 2; i < num + 2; ++i){
                    if (msg[i] == AUTH){
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return flag;
                }
                else {
                    flag = true;
                    return flag;
                }
            }
        }
        else if (state == CON){
            if (msg[1] != COMM){
                return flag;
            }
            else{
                if (msg[3] == IPv4 || msg[3] == DOM){
                    flag = true;
                    return flag;
                }
                else{
                    return flag;
                }
            }
        }
        return flag;
    }

    public ByteBuffer makeAcceptanceAnswer(){
        ByteBuffer buf = ByteBuffer.allocate(2);
        buf.put(VERS);
        buf.put(AUTH);
        return buf;
    }

    public ByteBuffer makeConnectAnswer(int port, boolean isConnected) {
        ByteBuffer buf = ByteBuffer.allocate(10);
        byte[] arr;
        if (isConnected) {
            arr = new byte[] {
                    VERS,
                    AUTH,
                    AUTH,
                    IPv4,
                    0x7F,
                    0x00,
                    0x00,
                    0x01,
                    (byte) ((port >> 8) & 0xFF), (byte) (port & 0xFF)};
        }
        else {
            arr = new byte[] {
                    VERS,
                    0x01,
                    AUTH,
                    IPv4,
                    0x7F,
                    0x00,
                    0x00,
                    0x01,
                    (byte) ((port >> 8) & 0xFF), (byte) (port & 0xFF)};
        }
        buf.put(arr);
        return buf;
    }
}